package com.bookstore.bookmanagement.book.dto;

public class BookExceptions {

    public static class BookAlreadyBorrowedException extends RuntimeException {
        public BookAlreadyBorrowedException(String isbn) {
            super("Book with ISBN " + isbn + " is already borrowed");
        }
    }

}
