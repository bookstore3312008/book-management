package com.bookstore.bookmanagement.book.dto;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BookEvents {

    @Value
    @Builder
    public static class BookCreated {

        @NonNull
        String title;

        @NonNull
        String author;

        @NonNull
        String isbn;

        @NonNull
        String category;

        String borrower;

    }

    @Value
    @Builder
    public static class BookRented {

        @NonNull
        String isbn;

        @NonNull
        String clientName;

    }

}
