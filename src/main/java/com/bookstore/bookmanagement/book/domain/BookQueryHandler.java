package com.bookstore.bookmanagement.book.domain;

import com.bookstore.bookmanagement.book.dto.BookApi;
import lombok.RequiredArgsConstructor;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class BookQueryHandler {

    private final BookRepository repository;

    Set<BookApi.Book> getAll() {
        return repository.findAll()
                .stream()
                .map(Book::toApi)
                .collect(Collectors.toSet());
    }

}
