package com.bookstore.bookmanagement.book.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.NoSuchElementException;

interface BookRepository extends MongoRepository<Book, String> {

    default Book findOrThrow(String isbn) {
        return findById(isbn)
                .orElseThrow(() -> new NoSuchElementException("Book with isbn: " + isbn + " not found"));
    }

}
