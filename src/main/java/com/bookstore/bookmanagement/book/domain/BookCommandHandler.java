package com.bookstore.bookmanagement.book.domain;

import com.bookstore.bookmanagement.book.dto.BookApi;
import com.bookstore.bookmanagement.book.dto.BookEvents;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;

@RequiredArgsConstructor
class BookCommandHandler {

    private final BookRepository repository;
    private final ApplicationEventPublisher eventPublisher;

    void create(BookApi.Book request) {
        Book book = Book.fromApi(request);
        repository.save(book);
        eventPublisher.publishEvent(book.toBookCreatedEvent());
    }

    void rent(BookApi.RentRequest rentRequest) {
        Book book = repository.findOrThrow(rentRequest.getIsbn());
        BookEvents.BookRented bookRentedEvent = book.rent(rentRequest.getClientName());
        repository.save(book);
        eventPublisher.publishEvent(bookRentedEvent);
    }

}
