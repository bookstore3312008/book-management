package com.bookstore.bookmanagement.book.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class BookConfiguration {

    private final BookRepository repository;
    private final ApplicationEventPublisher eventPublisher;

    @Bean
    BookFacade bookFacade() {
        return new BookFacade(bookCommandHandler(), bookQueryHandler());
    }

    @Bean
    BookCommandHandler bookCommandHandler() {
        return new BookCommandHandler(repository, eventPublisher);
    }

    @Bean
    BookQueryHandler bookQueryHandler() {
        return new BookQueryHandler(repository);
    }

}
