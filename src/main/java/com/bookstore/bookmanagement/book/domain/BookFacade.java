package com.bookstore.bookmanagement.book.domain;

import com.bookstore.bookmanagement.book.dto.BookApi;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class BookFacade {

    private final BookCommandHandler bookCommandHandler;
    private final BookQueryHandler bookQueryHandler;

    public Set<BookApi.Book> getAll() {
        return bookQueryHandler.getAll();
    }

    public void create(BookApi.Book book) {
        bookCommandHandler.create(book);
    }

    public void rent(BookApi.RentRequest rentRequest) {
        bookCommandHandler.rent(rentRequest);
    }

}
