package com.bookstore.bookmanagement.book.domain;

import com.bookstore.bookmanagement.book.dto.BookApi;
import com.bookstore.bookmanagement.book.dto.BookEvents;
import com.bookstore.bookmanagement.book.dto.BookExceptions;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;

@Document("book")
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PRIVATE)
class Book {

    @Id
    private String isbn;

    @NotNull
    private String title;

    @NotNull
    private String author;

    @NotNull
    private String category;

    private String borrower;

    static Book fromApi(BookApi.Book book) {
        return new Book(
                book.getIsbn(),
                book.getTitle(),
                book.getAuthor(),
                book.getCategory(),
                book.getBorrower()
        );
    }

    BookEvents.BookRented rent(String clientName) {
        checkBorrowerExists();
        borrower = clientName;
        return toBookRentedEvent(clientName);
    }

    private BookEvents.BookRented toBookRentedEvent(String clientName) {
        return BookEvents.BookRented.builder()
                .isbn(isbn)
                .clientName(clientName)
                .build();
    }

    private void checkBorrowerExists() {
        if (borrower != null) {
            throw new BookExceptions.BookAlreadyBorrowedException(isbn);
        }
    }

    BookApi.Book toApi() {
        return BookApi.Book.builder()
                .title(title)
                .author(author)
                .isbn(isbn)
                .category(category)
                .borrower(borrower)
                .build();
    }

    BookEvents.BookCreated toBookCreatedEvent() {
        return BookEvents.BookCreated.builder()
                .title(title)
                .author(author)
                .isbn(isbn)
                .category(category)
                .borrower(borrower)
                .build();
    }

}
