package com.bookstore.bookmanagement.book.infrastructure;

import com.bookstore.bookmanagement.book.domain.BookFacade;
import com.bookstore.bookmanagement.book.dto.BookApi;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
class BookController {

    private final BookFacade facade;

    @GetMapping
    Set<BookApi.Book> getAll() {
        return facade.getAll();
    }

    @PostMapping
    void create(@RequestBody @Valid BookApi.Book book) {
        facade.create(book);
    }

    @PutMapping
    void rent(@RequestBody @Valid BookApi.RentRequest rentRequest) {
        facade.rent(rentRequest);
    }

}
