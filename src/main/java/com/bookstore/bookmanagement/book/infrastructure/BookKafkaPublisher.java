package com.bookstore.bookmanagement.book.infrastructure;

import com.bookstore.bookmanagement.book.dto.BookEvents;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

@Slf4j
@RequiredArgsConstructor
class BookKafkaPublisher {

    private static final String BOOK_EVENTS_TOPIC = "book";
    private static final String EVENT_TYPE_HEADER_KEY = "eventType";
    private static final String BOOK_CREATED_EVENT = "BookCreated";
    private static final String BOOK_RENTED_EVENT = "BookRented";

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @EventListener
    @SneakyThrows
    void onBookCreatedEvent(BookEvents.BookCreated event) {
        log.info("Publishing BookCreatedEvent: {}", event);
        Message<String> message = toMessage(objectMapper.writeValueAsString(event), BOOK_CREATED_EVENT);
        kafkaTemplate.send(message);
    }

    @EventListener
    @SneakyThrows
    void onBookRentedEvent(BookEvents.BookRented event) {
        log.info("Publishing BookRentedEvent: {}", event);
        Message<String> message = toMessage(objectMapper.writeValueAsString(event), BOOK_RENTED_EVENT);
        kafkaTemplate.send(message);
    }

    private Message<String> toMessage(String objectMapper, String bookRentedEvent) {
        return MessageBuilder
                .withPayload(objectMapper)
                .setHeader(KafkaHeaders.TOPIC, BOOK_EVENTS_TOPIC)
                .setHeader(EVENT_TYPE_HEADER_KEY, bookRentedEvent)
                .build();
    }

}

