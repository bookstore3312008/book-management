package com.bookstore.bookmanagement.book.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
@RequiredArgsConstructor
class BookInfrastructureConfiguration {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @Bean
    BookKafkaPublisher bookKafkaPublisher() {
        return new BookKafkaPublisher(kafkaTemplate, objectMapper);
    }

}
